public class TicTacToeBoard {
	private String[][] board;
	private int size;
	private int marks;


	private int[] directions = { 
	//  x, y
		-1, 0, 	// left 
		1, 0,  	// right
		0, -1, 	// up 
		0, 1,  	// down
		-1, -1, // left-up
		1, 1,  	// right-down
		-1, 1,   // left-down
		1, -1   // right-up
	};

	public TicTacToeBoard (int n) {
		size = n;
		marks = size * size;
		// Size must be and odd number
		boolean isProperSize = (size % 2 == 1) && (size >= 3);
		if (!isProperSize) {
			String msg = "A tic-tac-toe game board must be square, have an odd number of cells on a side, " +
						 "and have at least 9 cells in total.";
			throw new IllegalArgumentException(msg);
		}
		board = new String[size][size];
		initializeBoard(board);
	}

	public boolean mark(int positionX, int positionY, String mark) throws DrawException, IllegalMoveException {
		if(board[positionX][positionY].equals("")){
			board[positionX][positionY] = mark;
			boolean won = win(positionX, positionY, mark);
			if (won)
				return won;

			marks--;
			if(marks == 0)
				throw new DrawException();

			return won;
		}
		throw new IllegalMoveException();
	}

	private void initializeBoard(String[][] b){
		for(int i = 0; i < size; i++)
			for(int j = 0; j < size; j++)
				board[i][j] = "";
	}

	private boolean win(int positionX, int positionY, String mark){
		for(int i = 0; i < directions.length; i += 4){
			int length = checkDirection(positionX, positionY, mark, directions[i], directions[i + 1])
						 + checkDirection(positionX, positionY, mark, directions[i + 2], directions[i + 3]);

			if (length == size)
				return true;
		}
		return false;
	}

	private int checkDirection(int positionX, int positionY, String mark, int directionX, int directionY){
		int length = 0;
		for(int i = positionX; i < size && i >= 0; i += directionX){		
			for(int j = positionY; j < size && j >= 0; j += directionY){
				if (board[i][j].equals(mark))
					length++;
				if(directionY == 0)
					break;
			}
			if(directionX == 0)
				break;
		}
		return length;
	}

	public void draw () {
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				String mark = board[i][j];
				if (mark.equals("")){
					mark = " ";
				}
				if (j == size - 1) {
					System.out.println(mark);
				} else {
					System.out.print(mark);
				}
			}
		}
	}
}
