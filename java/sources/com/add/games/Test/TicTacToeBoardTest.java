public class TicTacToeBoardTest {
	public static void main(String[] args){
		System.out.println("=== TESTS ===");
		boolean result;
		
		result = checkCannotConstructEvenSidedBoard();
		System.out.print("Fail to create board with even number of cells: ");
		if (result) {
			System.out.println("PASS");
		} else {
			System.out.println("FAIL");
		}

		result = checkCannotConstructTinyBoard();
		System.out.print("Fail to create board that is too small: ");
		if (result) {
			System.out.println("PASS");
		} else {
			System.out.println("FAIL");
		}

		result = checkCanConstructOddSidedBoard();
		System.out.print("Can create odd sided board: ");
		if (result) {
			System.out.println("PASS");
		} else {
			System.out.println("FAIL");
		}

		// Play a game
		TicTacToeBoard board = new TicTacToeBoard(3);
		board.draw();
		boolean won = false;
		String winner = "Error";
		int[] xxMoves = { 0, 1, 2 };
		int[] xyMoves = { 0, 0, 0 };
		int[] yxMoves = { 0, 1, 2 };
		int[] yyMoves = { 1, 1, 1 };
		int i = 0;
		while (!won){
			System.out.println(i);
			try {
				won = board.mark(xxMoves[i], xyMoves[i], "x");
				board.draw();
				if (won){
					winner = "x";
				} else {
					won = board.mark(yxMoves[i], yyMoves[i], "y");
					board.draw();
					if (won){
						winner = "y";
					}
				}
			} catch (IllegalMoveException e){
				System.out.println("Illegal move");
			} catch (DrawException e){
				System.out.println("Game is a draw");
			}
			i++;
			if (i > 2){
			 	break;
			}
		}
		System.out.println("The winner is: " + winner + " !!!!");

	}

	private static boolean checkCannotConstructEvenSidedBoard(){
		try {
			new TicTacToeBoard(6);
			return false;
		} catch (IllegalArgumentException e){
			return true;
		}
	}

	private static boolean checkCannotConstructTinyBoard(){
		try {
			new TicTacToeBoard(1);
			return false;
		} catch (IllegalArgumentException e){
			return true;
		}
	}

	private static boolean checkCanConstructOddSidedBoard(){
		try {
			new TicTacToeBoard(7);
			return true;
		} catch (IllegalArgumentException e){
			return false;
		}
	}
}