#!/usr/bin/python
# #############################################################################
# Author: Chris Beaton								             		      #
# Determine whether braces in a string are is_balanced                        #
###############################################################################

bracket_complements = {
	"{": "}",
	"}": "{",
	"[": "]",
	"]": "[",
	"(": ")",
	")": "("
}


def is_balanced(str):
	stack = []
	for c in str:
		top = None
		try:
			top = stack.pop()
		except:
			None
		if c in bracket_complements:		
			if top != bracket_complements[c]:
				if top == None:
					stack += [c]
				else:
					stack += [top, c]

	if len(stack) == 0:
		return True
	return False


inputs = {
	"a": {
		"string": "{} foo [] bar ()",
		"result": True
	},
	"b": {
		"string": "{] foo {] bar (",
		"result": False
	},
	"c": {
		"string": "[ bar [",
		"result": False
	},
	"d": {
		"string": "",
		"result": True
	}
}

for k in inputs:
	print ''
	input_string = inputs[k]["string"]
	expected_result = inputs[k]["result"]
	result = is_balanced(input_string)
	if result:
		print "'%s' is balanced" % input_string 
		print "expected result: %s" % expected_result
	else:
		print "'%s' is NOT balanced" % input_string
		print "expected result: %s" % expected_result
