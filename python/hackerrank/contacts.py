class Contacts(object):
	def __init__(self):
		self.contact_list = []


	def add(self, name):
		self.contact_list += [name]


	def find(self, partial):
	    partials_found = 0
		for contact in self.contact_list:
		    letter_matches = 0
		    for i in range(0, len(partial)):
		        if i in contact and partial[i] == contact[i]:
		            letter_matches += 1
            if letter_matches == len(partial):
                partials_found += 1
        print partials_found


c = Contacts()
c.add("hack")
c.add("hackerrank")
