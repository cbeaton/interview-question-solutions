class BinarySearchTreeNode(object):
	def __init__(self, d):
		self.data = d
		self.left = None
		self.right = None


	def insert(self, value):
		if value <= self.data:
			if self.left == None:
				self.left = BTreeNode(value)
			else:
				self.left.insert(value)
		else:
			if self.right == None:
				self.right = BTreeNode(value)
			else:
				self.right.insert(value)


	def print_inorder(self):
		if self.left != None:
			self.left.print_inorder()
		print self.data
		if self.right != None:
			self.right.print_inorder()


def check_is_binary_search_tree(node, minimum, maximum):
	if node.data < minimum or node.data > maximum:
		return False
	result = True
	if node.left != None:
		result = result and check_is_binary_search_tree(node.left, minimum, node.data - 1)
	if node.right != None:
		result = result and check_is_binary_search_tree(node.right, node.data + 1, maximum)
	return result


n = BTreeNode(0)
print check_is_binary_search_tree(n, 0, 10000)
n = BTreeNode(10000)
print check_is_binary_search_tree(n, 0, 10000)
n = BTreeNode(10)
print check_is_binary_search_tree(n, 0, 10000)
n.insert(4)
print check_is_binary_search_tree(n, 0, 10000)
n.insert(15)
print check_is_binary_search_tree(n, 0, 10000)
n.insert(5)
print check_is_binary_search_tree(n, 0, 10000)
n.insert(1)
print check_is_binary_search_tree(n, 0, 10000)
n.insert(12)
print check_is_binary_search_tree(n, 0, 10000)
n.insert(10000)
print check_is_binary_search_tree(n, 0, 10000)
n.insert(0)
print check_is_binary_search_tree(n, 0, 10000)
n = BTreeNode(4)
n.left = BTreeNode(2)
n.left.left = BTreeNode(1)
n.left.right = BTreeNode(3)
print check_is_binary_search_tree(n, 0, 10000)
n = BTreeNode(4)
n.right = BTreeNode(6)
n.right.left = BTreeNode(5)
n.right.right = BTreeNode(7)
print check_is_binary_search_tree(n, 0, 10000)
n = BTreeNode(4)
n.left = BTreeNode(2)
n.left.left = BTreeNode(1)
n.left.right = BTreeNode(3)
n.right = BTreeNode(6)
n.right.left = BTreeNode(5)
n.right.right = BTreeNode(7)
print check_is_binary_search_tree(n, 0, 10000)
n = BTreeNode(4)
n.left = BTreeNode(0)
print check_is_binary_search_tree(n, 0, 10000)
n = BTreeNode(4)
n.right = BTreeNode(10000)
print check_is_binary_search_tree(n, 0, 10000)
print ""

n = BTreeNode(4)
n.left = BTreeNode(4)
print check_is_binary_search_tree(n, 0, 10000)
n = BTreeNode(4)
n.right = BTreeNode(4)
print check_is_binary_search_tree(n, 0, 10000)
n = BTreeNode(4)
n.left = BTreeNode(5)
print check_is_binary_search_tree(n, 0, 10000)
n = BTreeNode(4)
n.right = BTreeNode(3)
print check_is_binary_search_tree(n, 0, 10000)
n = BTreeNode(4)
n.right = BTreeNode(6)
n.right.left = BTreeNode(7)
n.right.right = BTreeNode(5)
print check_is_binary_search_tree(n, 0, 10000)

# 2
# 1 2 4 3 5 6 7
n = BTreeNode(3)



