#!/usr/bin/python
#
# Compute the square root of a number
#
import math as m

COLUMN_FORMAT = "{0:f} {1:f} {2:f} {3:f} {4:d}"


def sqrt(integer, max_error):
	if integer < 0:
		raise ValueError("math domain error")

	sign = -1
	guess = integer / 2.0 
	diff = integer - (guess * guess)
	print "integer, guess, result, diff, sign"
	return recursive_sqrt(integer, guess, sign, max_error)


def recursive_sqrt(integer, guess, sign, max_error):
	result = guess * guess
	diff = integer - result
	print COLUMN_FORMAT.format(integer, guess, result, diff, sign)
	if diff > max_error:
		sign = sign * -1
		guess = guess - (diff / 2.0) * sign
		return recursive_sqrt(integer, guess, sign, max_error)
	return guess


merr = 0.001
test_values = [ i for i in range(0, 50)]
for v in test_values:
	print "Value: %d" % v 
	result = sqrt(v, merr);
	expected = m.sqrt(v);
	test = "FAIL"
	if (result - merr) <= expected and expected <= (result + merr):
		test = "PASS"
	print "{0:s} {1:s} {2:s} {3:s}".format("input", "result", "expected", test)
	print ""



sqrt(1, merr)

print "Raise ValueError for input < 0: "
try:
	print sqrt(-1, merr)
	print "FAIL"
except ValueError:
	print "PASS"

